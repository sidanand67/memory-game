const gameContainer = document.getElementById("game");

const leastMovesEl = document.getElementById("least-moves"); 

const movesEl = document.getElementById("moves"); 

const yesBtnEl = document.querySelector('.game-btn-grp > .yes-btn'); 

const noBtnEl = document.querySelector('.game-btn-grp > .no-btn'); 

const quitBtnEl = document.getElementById('quit-btn'); 

const quitYesBtnEl = document.querySelector('.quit-btn-grp > .yes-btn'); 

const quitNoBtnEl = document.querySelector('.quit-btn-grp > .no-btn'); 

const gameInfoEl = document.getElementById('game-info'); 

const gameMsgEl = document.getElementById('game-msg'); 

const gameBtnGrpEl = document.querySelector('.game-btn-grp'); 

const quitGameBtnGrpEl = document.querySelector('.quit-btn-grp'); 

const restartBtnEl = document.querySelector('.restart-btn'); 

const sucessBtnGrpEl = document.querySelector('.success-btn-grp'); 

let prev, current, moves, score; 

const IMAGES = [
    "1.gif",
    "2.gif",
    "3.gif",
    "4.gif",
    "5.gif",
    "6.gif",
    "7.gif",
    "8.gif",
    "9.gif",
    "10.gif",
    "11.gif",
    "12.gif",
    "1.gif",
    "2.gif",
    "3.gif",
    "4.gif",
    "5.gif",
    "6.gif",
    "7.gif",
    "8.gif",
    "9.gif",
    "10.gif",
    "11.gif",
    "12.gif",
];

if(localStorage.getItem('leastMoves') === null){
    localStorage.setItem('leastMoves', undefined); 
}

yesBtnEl.addEventListener("click", () => {
    gameInfoEl.style.display = "none"; 
    gameBtnGrpEl.style.display = "none"; 
    gameContainer.style.visibility = "visible"; 
    quitBtnEl.style.visibility = "visible"; 
}); 

noBtnEl.addEventListener('click', () => {
    gameMsgEl.textContent = "Adios 👋"; 
    gameBtnGrpEl.style.display = "none"; 
});

quitBtnEl.addEventListener("click", () => {
    gameContainer.style.visibility = "hidden"; 
    quitBtnEl.style.visibility = "hidden"; 
    gameInfoEl.style.display = "flex";
    gameMsgEl.textContent = "Do you want to Quit?"; 
    quitGameBtnGrpEl.style.display = "flex";  
}); 

quitYesBtnEl.addEventListener("click", () => {
    window.location.reload(); 
}); 

quitNoBtnEl.addEventListener("click", () => {
    quitBtnEl.style.visibility = "visible";
    gameInfoEl.style.display = "none";
    quitGameBtnGrpEl.style.display = "none"; 
    gameContainer.style.visibility = "visible";
}); 

restartBtnEl.addEventListener("click", () => {
    gameStart(); 
    gameInfoEl.style.display = "none";
    gameBtnGrpEl.style.display = "none";
    gameContainer.style.visibility = "visible";
    sucessBtnGrpEl.style.display = "flex"; 
    quitBtnEl.style.visibility = "visible"; 
}); 
// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

let shuffledImages = shuffle(IMAGES); 

function createDivsForImages(ImageArray){
    for (let image of ImageArray){
        const newDiv = document.createElement("div"); 
        
        const imageDiv = document.createElement("img"); 

        newDiv.append(imageDiv); 
        imageDiv.src = `/gifs/${image}`; 
        imageDiv.style.width = "100%"; 
        imageDiv.style.height = "100%"; 
        imageDiv.style.zIndex = "-1"; 
        imageDiv.style.borderRadius = "5px"; 

        newDiv.classList.toggle('hide'); 

        newDiv.classList.add(image.split('.')[0]);

        newDiv.addEventListener("click", handleCardClick); 

        gameContainer.append(newDiv); 
    }
}

// TODO: Implement this function
function handleCardClick(event){
    if (Array.from(event.target.classList).includes("hide")) {
        event.target.classList.toggle("hide");
        event.target.children[0].style.zIndex = "1";
        event.target.removeEventListener('click', handleCardClick); 
    }

    if (!prev){
        prev = event.target; 
        current = event.target; 
        moves++; 
        movesEl.textContent = `Your Moves: ${moves}`;
    }
    else {
        moves++; 
        movesEl.textContent = `Your Moves: ${moves}`;
        prev = current; 
        current = event.target;
        gameContainer.style.pointerEvents = "none";  
    
        if(prev.className === current.className){
            gameContainer.style.pointerEvents = "all"; 
            score++; 
            prev = undefined; 
            current = undefined; 

            if(score === 12){
                // console.log(score); 
                
                if (localStorage.getItem("leastMoves") === "undefined") {
                    localStorage.setItem("leastMoves", moves);
                } else {
                    if (Number(localStorage.getItem("leastMoves")) > moves) {
                        localStorage.setItem("leastMoves", moves);
                    }
                }

                leastMovesEl.textContent = `Least Moves: ${localStorage.getItem(
                    "leastMoves"
                )}`; 

                gameEnd(); 
            }
        }   
        else {
            setTimeout(() => {
                prev.children[0].style.zIndex = "-1";
                prev.classList.toggle("hide");
        
                current.children[0].style.zIndex = "-1";
                current.classList.toggle("hide");

                gameContainer.style.pointerEvents = "all";     
                prev.addEventListener('click', handleCardClick); 
                current.addEventListener('click', handleCardClick); 
                prev = undefined; 
                current = undefined; 
            }, 700);
            
        }
    }   
}

function gameStart(){
    moves = 0; 
    score = 0; 
    let leastMoves = localStorage.getItem('leastMoves') === "undefined" ? "XXX" : localStorage.getItem('leastMoves'); 
    leastMovesEl.textContent = `Least Moves: ${leastMoves}`; 

    createDivsForImages(shuffledImages); 
}

function gameEnd(){
    gameContainer.style.visibility = "hidden"; 
    gameInfoEl.style.display = "flex"; 
    gameMsgEl.textContent = `You completed the game in ${moves} moves.`; 
    quitBtnEl.style.visibility = "hidden"; 
    restartBtnEl.style.display = "block"; 
}

// when the DOM loads
gameStart(); 
